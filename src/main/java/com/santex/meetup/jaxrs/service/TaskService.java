package com.santex.meetup.jaxrs.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.santex.meetup.jaxrs.domain.Task;
import com.santex.meetup.jaxrs.repository.TaskRepository;

@Service
@Transactional
public class TaskService {

	@Autowired
	private TaskRepository taskRepository;

	public Task create(Task task) {
		return taskRepository.save(task);
	}

	public List<Task> getAll() {
		return taskRepository.findAll();
	}
}
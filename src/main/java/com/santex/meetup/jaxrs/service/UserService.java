package com.santex.meetup.jaxrs.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.santex.meetup.jaxrs.domain.User;
import com.santex.meetup.jaxrs.repository.UserRepository;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User create(User user) {
		return userRepository.save(user);
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public List<User> findByName(String name) {
		return userRepository.findByName(name);
	}

	public User findById(Long id) {
		return userRepository.findOne(id);
	}

}
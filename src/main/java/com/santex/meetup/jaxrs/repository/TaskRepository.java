package com.santex.meetup.jaxrs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.santex.meetup.jaxrs.domain.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {

}

package com.santex.meetup.jaxrs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.santex.meetup.jaxrs.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {

	public List<User> findByName(String name);
}

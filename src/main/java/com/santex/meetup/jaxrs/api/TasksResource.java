package com.santex.meetup.jaxrs.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.santex.meetup.jaxrs.domain.Task;
import com.santex.meetup.jaxrs.service.TaskService;

@Path("/tasks")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Component
public class TasksResource {

	@Autowired
	private TaskService taskService;

	@POST
	public Response createTask(Task task) {
		Task createdTask = taskService.create(task);

		return Response.status(Status.CREATED).entity(createdTask).build();
	}

	@GET
	public Response getAllTasks() {
		List<Task> taskList = taskService.getAll();

		return Response.ok().entity(taskList).build();
	}

}
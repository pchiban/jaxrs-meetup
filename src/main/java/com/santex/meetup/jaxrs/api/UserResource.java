package com.santex.meetup.jaxrs.api;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.santex.meetup.jaxrs.async.UserWorker;
import com.santex.meetup.jaxrs.domain.User;
import com.santex.meetup.jaxrs.service.UserService;

@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Component
public class UserResource {

	@Autowired
	private UserService userService;

	@POST
	public Response create(User user) {
		if (userService.findById(user.getId()) != null) {
			return Response.status(Status.CONFLICT).entity(user).build();
		}

		User createdUser = userService.create(user);
		return Response.status(Status.CREATED).entity(createdUser).build();
	}

	@GET
	@Transactional
	public Response findAll(@QueryParam("name") String name) {
		List<User> userList = null;

		if (name != null) {
			userList = userService.findByName(name);
		} else {
			userList = userService.findAll();
		}

		// trigger thread
		ExecutorService executor = Executors.newFixedThreadPool(1);
		for (User user : userList) {
			Hibernate.initialize(user.getTasks());
			executor.submit(new UserWorker(user));
		}

		return Response.ok().entity(userList).build();
	}

	@GET
	@Path("/{id}")
	public Response findById(@PathParam("id") Long id) {
		User user = userService.findById(id);

		if (user != null)
			return Response.ok().entity(user).build();
		else
			return Response.status(Status.NOT_FOUND).entity("User not found").build();
	}
}
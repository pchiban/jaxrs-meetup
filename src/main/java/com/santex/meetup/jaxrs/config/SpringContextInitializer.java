package com.santex.meetup.jaxrs.config;

import javax.servlet.FilterRegistration.Dynamic;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

@Order(Ordered.HIGHEST_PRECEDENCE)
public class SpringContextInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		WebApplicationContext rootAppContext = new AnnotationConfigWebApplicationContext();

		servletContext.setInitParameter("contextConfigLocation", "com.santex.meetup.jaxrs.config");

		// listener
		servletContext.addListener(new ContextLoaderListener(rootAppContext));

		// filters
//		Dynamic filter = servletContext.addFilter("OpenEntityManagerInViewFilter", OpenEntityManagerInViewFilter.class);
//		filter.setInitParameter("entityManagerFactoryBeanName", "entityManagerFactory");
//		filter.addMappingForUrlPatterns(null, true, "/*");
	}
}
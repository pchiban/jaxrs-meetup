package com.santex.meetup.jaxrs.config.persistence;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.santex.meetup.jaxrs.config.ApplicationConfig;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.santex.meetup.jaxrs.repository" })
class PersistenceContext {

	@Autowired
	private ApplicationConfig config;

	@Bean(destroyMethod = "close")
	public DataSource dataSource() throws IllegalStateException, PropertyVetoException {
		ComboPooledDataSource datasource = new ComboPooledDataSource();
		datasource.setDriverClass(config.getProperty("db.driver"));
		datasource.setJdbcUrl(config.getProperty("db.url"));
		datasource.setUser(config.getProperty("db.username"));
		datasource.setPassword(config.getProperty("db.password"));

		// configure pool
		datasource.setMinPoolSize(10);
		datasource.setMaxPoolSize(50);
		datasource.setCheckoutTimeout(100);
		datasource.setMaxStatements(50);
		datasource.setIdleConnectionTestPeriod(1000);

		return datasource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, Environment env) {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactoryBean.setPackagesToScan("com.santex.meetup.jaxrs.domain");
		entityManagerFactoryBean.setPersistenceUnitName("entityManager");

		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
		jpaProperties.put("hibernate.hbm2ddl.auto", env.getRequiredProperty("hibernate.hbm2ddl.auto"));
		jpaProperties.put("hibernate.ejb.naming_strategy", env.getRequiredProperty("hibernate.ejb.naming_strategy"));
		jpaProperties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
		jpaProperties.put("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));

		entityManagerFactoryBean.setJpaProperties(jpaProperties);

		return entityManagerFactoryBean;
	}

	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);

		return transactionManager;
	}
}
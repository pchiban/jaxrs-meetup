package com.santex.meetup.jaxrs.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.santex.meetup.jaxrs")
public class SpringConfig {
}
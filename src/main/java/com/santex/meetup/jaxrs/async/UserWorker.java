package com.santex.meetup.jaxrs.async;

import javax.transaction.Transactional;

import com.santex.meetup.jaxrs.domain.User;

public class UserWorker implements Runnable {

	private User user;

	public UserWorker(User user) {
		this.user = user;
	}

	@Override
	@Transactional
	public void run() {
		try {
			// process
			long threadId = Thread.currentThread().getId();
			System.out.println(String.format("Starting thread %s process for user [%s]", threadId, user));
			System.out.println(String.format("Thread %s, User %s", threadId, user));
			System.out.println(String.format("Leaving thread %s", threadId));
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

}

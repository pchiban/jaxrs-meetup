# JAXRS-MEETUP
=======================

## A kickstart project based on Jersey 2, Spring 4, Servlet 3.1 (annotation-based, no XMLs).

## Data base:
- MySql: create an schema called "jaxrs-meetup"
- Code: update application.properties with your database credentials (user/pwd)

## Application:
- Startup: mvn tomcat7:run

## Endpoints: 
- http://localhost:8080/jaxrs-meetup/api/users
- http://localhost:8080/jaxrs-meetup/api/tasks

=======================
=======================